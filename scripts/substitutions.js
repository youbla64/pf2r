let babeleKeepOriginalName = false;

export function renameItems() {
    babeleKeepOriginalName = game.settings.get('pf2e-ru', 'babeleItemKeepOriginalName');
    // пропущен Earthbreaker, Feng Huo Lun, Flying Talon, Gauntlet Bow, Lancer, Mikazuki, Phalanx Piercer, Scizore, Spraysling, Triggerbrand, Wrecker, Wrist Launcher, Zulfikar

    const weapons = {
        "adze": "n",
        "air-repeater": "f",
        "aklys": "m",
        "alchemical-bomb": "f",
        "alchemical-crossbow": "m",
        "aldori-dueling-sword": "m",
        "arbalest": "m",
        "arquebus": "f",
        "asp-coil": "m",
        "atlatl": "m",
        "asp-coil": "m",
        "axe-musket": "m",
        "barricade-buster": "m",
        "bastard-sword": "m",
        "battle-axe": "m",
        "battle-lute": "f",
        "battle-saddle": "n",
        "bec-de-corbin": "f",
        "big-boom-gun": "f",
        "black-powder-knuckle-dusters": "p",
        "bladed-diabolo": "n",
        "bladed-gauntlet": "f",
        "bladed-hoop": "m",
        "bladed-scarf": "m",
        "blowgun": "f",
        "blowgun-darts": "p",
        "blunderbuss": "m",
        "bo-staff": "m",
        "boarding-axe": "m",
        "boarding-pike": "f",
        "bola": "f",
        "boomerang": "m",
        "bow-staff": "m",
        "breaching-pike": "f",
        "broadspear": "n",
        "butchering-axe": "m",
        "butterfly-sword": "m",
        "buugeng": "m",
        "cane-pistol": "m",
        "capturing-spetum": "m",
        "chain-sword": "m",
        "chakram": "m",
        "chakri": "m",
        "clan-dagger": "m",
        "clan-pistol": "m",
        "claw": "m",
        "claw-blade": "m",
        "club": "f",
        "coat-pistol": "m",
        "combat-fishing-pole": "f",
        "combat-grapnel": "m",
        "combat-lure": "f",
        "composite-longbow": "m",
        "composite-shortbow": "m",
        "corset-knife": "m",
        "crescent-cross": "m",
        "crossbow": "m",
        "dagger": "m",
        "dagger-pistol": "m",
        "daikyu": "m",
        "dancers-spear": "n",
        "dandpatta": "f",
        "dart": "m",
        "dogslicer": "m",
        "donchak": "m",
        "double-barreled-musket": "m",
        "double-barreled-pistol": "m",
        "dragon-mouth-pistol": "m",
        "dueling-pistol": "m",
        "dueling-spear": "n",
        "dwarven-dorn-dergar": "m",
        "dwarven-scattergun": "n",
        "dwarven-war-axe": "f",
        "elven-branched-spear": "n",
        "elven-curve-blade": "f",
        "explosive-dogslicer": "m",
        "exquisite-sword-cane": "f",
        "exquisite-sword-cane-sheath": "p",
        "falcata": "f",
        "falchion": "m",
        "fangwire": "m",
        "fauchard": "m",
        "fighting-fan": "m",
        "fighting-stick": "f",
        "filchers-fork": "f",
        "fire-lance": "n",
        "fire-poi": "m",
        "fist": "m",
        "flail": "m",
        "flingflenser": "m",
        "flintlock-musket": "m",
        "flintlock-pistol": "m",
        "flyssa": "f",
        "forked-bipod": "f",
        "frying-pan": "f",
        "gada": "f",
        "gaff": "m",
        "gakgung": "m",
        "gauntlet": "f",
        "gill-hook": "m",
        "glaive": "f",
        "gnome-amalgam-musket": "m",
        "gnome-flickmace": "f",
        "gnome-hooked-hammer": "m",
        "greataxe": "f",
        "greatclub": "f",
        "greatpick": "m",
        "greatsword": "m",
        "griffon-cane": "f",
        "guisarme": "f",
        "gun-sword": "m",
        "halberd": "f",
        "halfling-sling-staff": "f",
        "hammer-gun": "m",
        "hand-adze": "n",
        "hand-cannon": "f",
        "hand-crossbow": "m",
        "harmona-gun": "n",
        "harpoon": "m",
        "hatchet": "m",
        "heavy-crossbow": "m",
        "hongali-hornbow": "m",
        "hook-sword": "p",
        "horsechopper": "m",
        "injection-spear": "n",
        "javelin": "n",
        "jaws": "p",
        "jezail": "m",
        "jiu-huan-dao": "m",
        "juggling-club": "f",
        "kalis": "m",
        "kama": "f",
        "karambit": "m",
        "katana": "f",
        "katar": "m",
        "khakkhara": "f",
        "khopesh": "m",
        "knuckle-duster": "m",
        "kris": "m",
        "kukri": "m",
        "kusarigama": "f",
        "lance": "f",
        "leiomano": "f",
        "light-hammer": "m",
        "light-mace": "f",
        "light-pick": "m",
        "lion-scythe": "f",
        "long-air-repeater": "f",
        "long-hammer": "m",
        "longbow": "m",
        "longspear": "n",
        "longsword": "m",
        "mace": "f",
        "mace-multipistol": "f",
        "machete": "m",
        "main-gauche": "m",
        "mambele": "f",
        "maul": "f",
        "meteor-hammer": "m",
        "mithral-tree": "n",
        "monkeys-fist": "n",
        "morningstar": "m",
        "naginata": "f",
        "nightstick": "f",
        "nine-ring-sword": "m",
        "nodachi": "m",
        "nunchaku": "p",
        "ogre-hook": "m",
        "orc-knuckle-dagger": "m",
        "orc-necksplitter": "m",
        "panabas": "m",
        "pepperbox": "f",
        "pick": "m",
        "piercing-wind": "m",
        "piranha-kiss": "m",
        "poi": "p",
        "polytool": "m",
        "probing-cane": "f",
        "ranseur": "f",
        "rapier": "f",
        "rapier-pistol": "m",
        "reinforced-wheels": "p",
        "reinforced-stock": "m",
        "repeating-crossbow": "m",
        "repeating-hand-crossbow": "m",
        "repeating-heavy-crossbow": "m",
        "rhoka-sword": "m",
        "rope-dart": "m",
        "rotary-bow": "m",
        "rungu": "m",
        "sai": "m",
        "sansetsukon": "m",
        "sap": "f",
        "sawtooth-saber": "f",
        "scimitar": "m",
        "scorpion-whip": "m",
        "scourge": "f",
        "scythe": "f",
        "shauth-lash": "f",
        "shears": "p",
        "shield-bash": "m",
        "shield-boss": "m",
        "shield-bow": "m",
        "shield-pistol": "m",
        "shield-spikes": "p",
        "shobhad-longrifle": "m",
        "shortbow": "m",
        "shortsword": "m",
        "shuriken": "m",
        "sickle": "m",
        "sickle-saber": "f",
        "slide-pistol": "m",
        "sling": "f",
        "sling-bullets": "p",
        "spear": "n",
        "spiked-chain": "f",
        "spiked-gauntlet": "f",
        "spiral-rapier": "f",
        "spoon-gun": "m",
        "staff": "m",
        "starknife": "m",
        "stiletto-pen": "f",
        "sukgung": "m",
        "sun-sling": "f",
        "switchscythe": "f",
        "sword-cane": "m",
        "talwar": "m",
        "tamchal-chakram": "m",
        "taw-launcher": "m",
        "tekko-kagi": "m",
        "temple-sword": "m",
        "tengu-gale-blade": "m",
        "thorn-whip": "m",
        "three-peaked-tree": "n",
        "three-section-naginata": "f",
        "throwing-knife": "m",
        "thunder-sling": "f",
        "thundermace": "f",
        "tonfa": "f",
        "tri-bladed-katar": "m",
        "tricky-pick": "f",
        "trident": "m",
        "urumi": "m",
        "visap": "m",
        "wakizashi": "m",
        "war-flail": "m",
        "war-lance": "f",
        "war-razor": "f",
        "warhammer": "m",
        "wheel-blades": "p",
        "wheel-spikes": "p",
        "whip": "m",
        "whip-claw": "m",
        "whip-staff": "m",
        "wish-blade": "m",
        "wish-knife": "m",
        "wooden-taws": "p"
    };
    // все щиты - m

    // кроме Niyaháat, O-Yoroi, Sankeit
    const armor = {
        "armored-cloak": "m",
        "armored-coat": "n",
        "bastion-plate": "p",
        "breastplate": "m",
        "buckle-armor": "m",
        "ceramic-plate": "p",
        "chain-mail": "f",
        "chain-shirt": "f",
        "coral-armor": "m",
        "explorers-clothing": "f",
        "fortress-plate": "p",
        "full-plate": "m",
        "gi": "m",
        "half-plate": "m",
        "hellknight-breastplate": "m",
        "hellknight-half-plate": "m",
        "hellknight-plate": "m",
        "hide-armor": "m",
        "lamellar-breastplate": "m",
        "lattice-armor": "m",
        "leaf-weave": "m",
        "leather-armor": "m",
        "leather-lamellar": "m",
        "mantis-shell": "m",
        "niyahaat": "m",
        "o-yoroi": "m",
        "padded-armor": "m",
        "power-suit": "m",
        "quilted-armor": "m",
        "rattan-armor": "m",
        "sankeit": "m",
        "scale-mail": "m",
        "scroll-robes": "f",
        "splint-mail": "m",
        "studded-leather-armor": "m",
        "subterfuge-suit": "m",
        "wooden-breastplate": "m"
    }

    // замена материалов
    // пропущены sisterstone
    const materials = {
        abysium: {
            label: 'абисиумн{ый-ая-ое-ые}'
        },
        adamantine: {
            label: 'адамантинов{ый-ая-ое-ые}'
        },
        'cold-iron': {
            label: 'из холодного железа',
            postfix: true
        },
        dawnsilver: {
            label: 'из рассветного серебра',
            postfix: true
        },
        djezet: {
            label: 'джезетов{ый-ая-ое-ые}'
        },
        duskwood: {
            label: 'из сумеречного дерева',
            postfix: true
        },
        inubrix: {
            label: 'инубриксов{ый-ая-ое-ые}'
        },
        'keep-stone': {
            label: 'из крепостного камня',
            postfix: true
        },
        noqual: {
            label: 'ноквалов{ый-ая-ое-ые}'
        },
        peachwood: {
            label: 'из персикового дерева',
            postfix: true
        },
        orichalcum: {
            label: 'орихалков{ый-ая-ое-ые}'
        },
        siccatite: {
            label: 'сиккатитов{ый-ая-ое-ые}'
        },
        silver: {
            label: 'серебрян{ый-ая-ое-ые}'
        },
        sloughstone: {
            label: 'экзувиелитн{ый-ая-ое-ые}'
        },
        'sovereign-steel': {
            label: 'из суверенной стали',
            postfix: true
        },
        warpglass: {
            label: 'из искривленного стекла',
            postfix: true
        }
    }

    // замена рун
    const runes = {
        striking: {
            striking: {
                label: "разящ{ий-ая-ее-ие}"
            },
            greaterStriking: {
                label: "отлично разящ{ий-ая-ее-ие}"
            },
            majorStriking: {
                label: "сильно разящ{ий-ая-ее-ие}"
            },
            mythicStriking: {
                label: "мифически разящ{ий-ая-ее-ие}"
            }
        },
        resilient: {
            resilient: {
                label: "стойк{ий-ая-ее-ие}"
            },
            greaterResilient: {
                label: "отлично стойк{ий-ая-ее-ие}"
            },
            majorResilient: {
                label: "сильно стойк{ий-ая-ее-ие}"
            },
            mythicResilient: {
                label: "мифически стойк{ий-ая-ее-ие}"
            }
        },
        reinforcing: {
            1: {
                label: "слабо упрочнённ{ый-ая-ое-ые}"
            },
            2: {
                label: "мало упрочнённ{ый-ая-ое-ые}"
            },
            3: {
                label: "средне упрочнённ{ый-ая-ое-ые}"
            },
            4: {
                label: "отлично упрочнённ{ый-ая-ое-ые}"
            },
            5: {
                label: "превосходно упрочнённ{ый-ая-ое-ые}"
            },
            6: {
                label: "высше упрочнённ{ый-ая-ое-ые}"
            },
        },
        property: {
            weapon: {
                ancestralEchoing: {
                    label: "наставляющ{ий-ая-ее-ие}"
                },
                anchoring: {
                    label: "якорн{ый-ая-ое-ые}"
                },
                greaterAnchoring: {
                    label: "отлично якорн{ый-ая-ое-ые}"
                },
                ashen: {
                    label: "пепельн{ый-ая-ое-ые}"
                },
                greaterAshen: {
                    label: "отлично пепельн{ый-ая-ое-ые}"
                },
                astral: {
                    label: "астральн{ый-ая-ое-ые}"
                },
                greaterAstral: {
                    label: "отлично астральн{ый-ая-ое-ые}"
                },
                authorized: {
                    label: "авторизованн{ый-ая-ое-ые}"
                },
                bane: {
                    label: "гибельн{ый-ая-ое-ые}"
                },
                bloodbane: {
                    label: "кровной погибели",
                    postfix: true
                },
                greaterBloodbane: {
                    label: "отличной кровной погибели",
                    postfix: true
                },
                bloodthirsty: {
                    label: "кровожадн{ый-ая-ое-ые}"
                },
                brilliant: {
                    label: "сверкающ{ий-ая-ее-ие}"
                },
                greaterBrilliant: {
                    label: "отлично сверкающ{ий-ая-ее-ие}"
                },
                called: {
                    label: "вызываем{ый-ая-ое-ые}"
                },
                coating: {
                    label: "покрывающ{ий-ая-ее-ие}ся"
                },
                conducting: {
                    label: "пропускающ{ий-ая-ее-ие}"
                },
                corrosive: {
                    label: "разъедающ{ий-ая-ее-ие}"
                },
                greaterCorrosive: {
                    label: "отлично разъедающ{ий-ая-ее-ие}"
                },
                crushing: {
                    label: "сокрушающ{ий-ая-ее-ие}"
                },
                greaterCrushing: {
                    label: "отлично сокрушающ{ий-ая-ее-ие}"
                },
                cunning: {
                    label: "хитр{ый-ая-ое-ые}"
                },
                dancing: {
                    label: "танцующ{ий-ая-ее-ие}"
                },
                deathdrinking: {
                    label: "упивающ{ий-ая-ее-ие}ся смертью"
                },
                decaying: {
                    label: "увядающ{ий-ая-ее-ие}"
                },
                greaterDecaying: {
                    label: "отлично увядающ{ий-ая-ее-ие}"
                },
                demolishing: {
                    label: "сносящ{ий-ая-ее-ие}"
                },
                disrupting: {
                    label: "жизненн{ый-ая-ое-ые}"
                },
                greaterDisrupting: {
                    label: "отлично жизненн{ый-ая-ое-ые}"
                },
                earthbinding: {
                    label: "приземляющ{ий-ая-ее-ие}"
                },
                energizing: {
                    label: "заряжающ{ий-ая-ее-ие}ся"
                },
                extending: {
                    label: "удлиняем{ый-ая-ое-ые}"
                },
                greaterExtending: {
                    label: "отлично удлиняем{ый-ая-ое-ые}"
                },
                fanged: {
                    label: "клыкаст{ый-ая-ое-ые}"
                },
                greaterFanged: {
                    label: "отлично клыкаст{ый-ая-ое-ые}"
                },
                flickering: {
                    label: "мерцающ{ий-ая-ее-ие}"
                },
                majorFanged: {
                    label: "превосходно клыкаст{ый-ая-ое-ые}"
                },
                fearsome: {
                    label: "устрашающ{ий-ая-ее-ие}"
                },
                greaterFearsome: {
                    label: "отлично устрашающ{ий-ая-ее-ие}"
                },
                flaming: {
                    label: "огненн{ый-ая-ое-ые}"
                },
                greaterFlaming: {
                    label: "отлично огненн{ый-ая-ое-ые}"
                },
                flurrying: {
                    label: "шквальн{ый-ая-ое-ые}"
                },
                frost: {
                    label: "морозн{ый-ая-ое-ые}"
                },
                greaterFrost: {
                    label: "отлично морозн{ый-ая-ое-ые}"
                },
                ghostTouch: {
                    label: "призрачного касания",
                    postfix: true
                },
                giantKilling: {
                    label: "убийства гигантов",
                    postfix: true
                },
                greaterGiantKilling: {
                    label: "отличного убийства гигантов",
                    postfix: true
                },
                grievous: {
                    label: "мучающ{ий-ая-ее-ие}"
                },
                hauling: {
                    label: "перетягивающ{ий-ая-ее-ие}"
                },
                greaterHauling: {
                    label: "отлично перетягивающ{ий-ая-ее-ие}"
                },
                holy: {
                    label: "свят{ой-ая-ое-ые}"
                },
                hooked: {
                    label: "крючковат{ый-ая-ое-ые}"
                },
                hopeful: {
                    label: "дающ{ий-ая-ее-ие} надежду"
                },
                impactful: {
                    label: "ударн{ый-ая-ое-ые}"
                },
                greaterImpactful: {
                    label: "отлично ударн{ый-ая-ое-ые}"
                },
                impossible: {
                    label: "невозможн{ый-ая-ое-ые}"
                },
                keen: {
                    label: "остр{ый-ая-ое-ые}"
                },
                kinWarding: {
                    label: "родовой защиты",
                    postfix: true
                },
                rooting: {
                    label: "укореняющ{ий-ая-ее-ие}"
                },
                greaterRooting: {
                    label: "отлично укореняющ{ий-ая-ее-ие}"
                },
                majorRooting: {
                    label: "превосходно укореняющ{ий-ая-ее-ие}"
                },
                trueRooting: {
                    label: "истинно укореняющ{ий-ая-ее-ие}"
                },
                merciful: {
                    label: "милосердн{ый-ая-ое-ые}"
                },
                nightmare: {
                    label: "кошмарн{ый-ая-ое-ые}"
                },
                pacifying: {
                    label: "умиротворяющ{ий-ая-ее-ие}"
                },
                quickstrike: {
                    label: "быстрого удара",
                    postfix: true
                },
                returning: {
                    label: "возвращающ{ий-ая-ее-ие}ся"
                },
                serrating: {
                    label: "зазубренн{ый-ая-ое-ые}"
                },
                shifting: {
                    label: "изменчив{ый-ая-ое-ые}"
                },
                shock: {
                    label: "шоков{ый-ая-ое-ые}"
                },
                greaterShock: {
                    label: "отлично шоков{ый-ая-ое-ые}"
                },
                shockwave: {
                    label: "ударной волны",
                    postfix: true
                },
                spellStoring: {
                    label: "хранящ{ий-ая-ее-ие} заклинания"
                },
                swarming: {
                    label: "роящ{ий-ая-ее-ие}ся"
                },
                thundering: {
                    label: "грохочущ{ий-ая-ее-ие}"
                },
                greaterThundering: {
                    label: "отлично грохочущ{ий-ая-ее-ие}"
                },
                underwater: {
                    label: "подводн{ый-ая-ое-ые}"
                },
                unholy: {
                    label: "нечестив{ый-ая-ое-ые}"
                },
                vorpal: {
                    label: "стрижающ{ий-ая-ее-ие}"
                },
                wounding: {
                    label: "ранящ{ий-ая-ее-ие}"
                },
            },
            armor: {
                acidResistant: {
                    label: "сопротивления кислоте",
                    postfix: true
                },
                greaterAcidResistant: {
                    label: "отличного сопротивления кислоте",
                    postfix: true
                },
                advancing: {
                    label: "продвигающ{ий-ая-ее-ие}"
                },
                greaterAdvancing: {
                    label: "отлично продвигающ{ий-ая-ее-ие}"
                },
                aimAiding: {
                    label: "нацеливающ{ий-ая-ее-ие}"
                },
                antimagic: {
                    label: "антимагическ{ий-ая-ое-ие}"
                },
                assisting: {
                    label: "опорн{ый-ая-ое-ые}"
                },
                bitter: {
                    label: "горьк{ий-ая-ое-ие}"
                },
                coldResistant: {
                    label: "сопротивления холоду",
                    postfix: true
                },
                greaterColdResistant: {
                    label: "отличного сопротивления холоду",
                    postfix: true
                },
                deathless: {
                    label: "бессмертн{ый-ая-ое-ые}"
                },
                electricityResistant: {
                    label: "сопротивления электричеству",
                    postfix: true
                },
                greaterElectricityResistant: {
                    label: "отличного сопротивления электричеству",
                    postfix: true
                },
                energyAdaptive: {
                    label: "энергетической адаптации",
                    postfix: true
                },
                etherial: {
                    label: "эфирн{ый-ая-ое-ые}"
                },
                lesserDread: {
                    label: "ужасн{ый-ая-ое-ые}"
                },
                moderateDread: {
                    label: "умеренно ужасн{ый-ая-ое-ые}"
                },
                greaterDread: {
                    label: "отлично ужасн{ый-ая-ое-ые}"
                },
                fireResistant: {
                    label: "сопротивления огню",
                    postfix: true
                },
                greaterFireResistant: {
                    label: "отличного сопротивления огню",
                    postfix: true
                },
                fortification: {
                    label: "укрепленн{ый-ая-ое-ые}"
                },
                greaterFortification: {
                    label: "отлично укрепленн{ый-ая-ое-ые}"
                },
                glamered: {
                    label: "гламурн{ый-ая-ое-ые}"
                },
                gliding: {
                    label: "скользящ{ий-ая-ее-ие}"
                },
                immovable: {
                    label: "неподвижн{ый-ая-ое-ые}"
                },
                implacable: {
                    label: "неумолим{ый-ая-ое-ые}"
                },
                invisibility: {
                    label: "невидим{ый-ая-ое-ые}"
                },
                greaterInvisibility: {
                    label: "отлично невидим{ый-ая-ое-ые}"
                },
                magnetizing: {
                    label: "магнитн{ый-ая-ое-ые}"
                },
                malleable: {
                    label: "податлив{ый-ая-ое-ые}"
                },
                misleading: {
                    label: "обманчив{ый-ая-ое-ые}"
                },
                portable: {
                    label: "переносн{ой-ая-ое-ые}"
                },
                quenching: {
                    label: "тушащ{ий-ая-ее-ие}"
                },
                greaterQuenching: {
                    label: "отлично тушащ{ий-ая-ее-ие}"
                },
                majorQuenching: {
                    label: "превосходно тушащ{ий-ая-ее-ие}"
                },
                trueQuenching: {
                    label: "истинно тушащ{ий-ая-ее-ие}"
                },
                raiment: {
                    label: "облачающ{ий-ая-ее-ие}"
                },
                ready: {
                    label: "готовности",
                    postfix: true
                },
                greaterReady: {
                    label: "отличной готовности",
                    postfix: true
                },
                rockBraced: {
                    label: "укреплённ{ый-ая-ое-ые} камнем"
                },
                shadow: {
                    label: "тенев{ой-ая-ое-ые}"
                },
                greaterShadow: {
                    label: "отлично тенев{ой-ая-ое-ые}"
                },
                majorShadow: {
                    label: "превосходно тенев{ой-ая-ое-ые}"
                },
                sinisterKnight: {
                    label: "зловещего рыцаря",
                    postfix: true
                },
                sizeChanging: {
                    label: "смены размера",
                    postfix: true
                },
                slick: {
                    label: "скользк{ий-ая-ее-ие}"
                },
                greaterSlick: {
                    label: "отлично скользк{ий-ая-ее-ие}"
                },
                majorSlick: {
                    label: "превосходно скользк{ий-ая-ее-ие}"
                },
                soaring: {
                    label: "парящ{ий-ая-ее-ие}"
                },
                stanching: {
                    label: "кровоостанавливающ{ий-ая-ее-ие}"
                },
                greaterStanching: {
                    label: "отлично кровоостанавливающ{ий-ая-ее-ие}"
                },
                majorStanching: {
                    label: "превосходно кровоостанавливающ{ий-ая-ее-ие}"
                },
                trueStanching: {
                    label: "истинно кровоостанавливающ{ий-ая-ее-ие}"
                },
                swallowSpike: {
                    label: "колюч{ий-ая-ее-ие}"
                },
                greaterSwallowSpike: {
                    label: "отлично колюч{ий-ая-ее-ие}"
                },
                majorSwallowSpike: {
                    label: "превосходно колюч{ий-ая-ее-ие}"
                },
                winged: {
                    label: "крылат{ый-ая-ое-ые}"
                },
                greaterWinged: {
                    label: "отлично крылат{ый-ая-ое-ые}"
                },
            }
        }
    }

    function toCamelCase(s) {
        s = s.split('-');
        return s.map(c => c[0].toUpperCase() + c.slice(1)).reduce((a, b) => a + b, '');
    }

    function adaptiveReplace(name, seek, replaceWith, g) {
        seek = game.i18n.localize(seek);
        const newPart = replaceWith.label
            .replace(/\{(?<m>[^-]+)-(?<f>[^-]+)-(?<n>[^-]+)-(?<p>[^-]+)\}/, (_m, _p1, _p2, _p3, _p4, _offset, _string, groups) => groups[g]);
        if (replaceWith.postfix) {
            return name.replace(seek, '') + ' ' + newPart;
        }
        else {
            return name.replace(seek, newPart);
        }
    }

    libWrapper.register('pf2e-ru', 'game.pf2e.system.generateItemName', (wrapped, item) => {
        if (!['weapon', 'armor', 'shield'].includes(item.type))
            return wrapped(item);

        const baseItemDictionary = {
            armor: CONFIG.PF2E.baseArmorTypes,
            shield: CONFIG.PF2E.baseShieldTypes,
            weapon: CONFIG.PF2E.baseWeaponTypes
        }

        let originalName = item._source.name;
        let tempName = originalName;

        // Если сохраняем оригинальное название при импорте, убираем его здесь
        if (babeleKeepOriginalName)
            tempName = tempName.split('/')[0].trim();

        // Уберем звездочки
        tempName = tempName.replace('(*)', '');
        const baseItem = item.system.baseItem;
        if (baseItem && baseItemDictionary[item.type][baseItem]) {
            const baseName = game.i18n.localize(baseItemDictionary[item.type][baseItem]);
            if (SearchFilter.cleanQuery(tempName.toLowerCase()) == SearchFilter.cleanQuery(baseName.toLowerCase()))
                tempName = baseName;
        }

        item._source.name = tempName;
        item.name = tempName;

        let genName = wrapped(item);
        item._source.name = originalName;
        item.name = originalName;
        if (genName === tempName) {
            return originalName;
        }
        item._source.name = originalName;

        let g = item.type == 'weapon' ? weapons[item.baseType] : item.type == 'armor' ? armor[item.baseType] : item.type == 'shield' ? 'm' : null;
        if (!g)
            return genName;

        // заменим фундаментальные руны
        if (item.type === 'weapon' && item.system.runes.striking) {
            const striking = item.system.runes.striking;
            if (striking == 1) { genName = adaptiveReplace(genName, 'PF2E.Item.Weapon.Rune.Striking.Striking', runes.striking.striking, g); }
            else if (striking == 2) { genName = adaptiveReplace(genName, 'PF2E.Item.Weapon.Rune.Striking.Greater', runes.striking.greaterStriking, g); }
            else if (striking == 3) { genName = adaptiveReplace(genName, 'PF2E.Item.Weapon.Rune.Striking.Major', runes.striking.majorStriking, g); }
            else if (striking == 4) { genName = adaptiveReplace(genName, 'PF2E.Item.Weapon.Rune.Striking.Mythic', runes.striking.mythicStriking, g); }
        }
        else if (item.type === 'armor' && item.system.runes.resilient) {
            const resilient = item.system.runes.resilient;
            if (resilient == 1) { genName = adaptiveReplace(genName, 'PF2E.ArmorResilientRune', runes.resilient.resilient, g); }
            else if (resilient == 2) { genName = adaptiveReplace(genName, 'PF2E.ArmorGreaterResilientRune', runes.resilient.greaterResilient, g); }
            else if (resilient == 3) { genName = adaptiveReplace(genName, 'PF2E.ArmorMajorResilientRune', runes.resilient.majorResilient, g); }
            else if (resilient == 4) { genName = adaptiveReplace(genName, 'PF2E.ArmorMythicResilientRune', runes.resilient.mythicResilient, g); }
        }
        else if (item.type === 'shield' && item.system.runes.reinforcing) {
            const grade = item.system.runes.reinforcing;
            const reinforcingLabels = [null, "Minor", "Lesser", "Moderate", "Greater", "Major", "Supreme"];
            const originalRuneLabel = `PF2E.Item.Shield.Rune.Reinforcing.${reinforcingLabels[grade]}`;
            genName = adaptiveReplace(genName, originalRuneLabel, runes.reinforcing[grade], g);
        }

        // заменим руны свойств
        if (item.system.runes.property?.length > 0) {
            if (item.type === 'weapon') {
                for (const propRune of item.system.runes.property) {
                    if (propRune in runes.property.weapon) {
                        genName = adaptiveReplace(genName, `PF2E.WeaponPropertyRune.${propRune}.Name`, runes.property.weapon[propRune], g);
                    }
                }
            }
            else if (item.type === 'armor') {
                for (const propRune of item.system.runes.property) {
                    if (propRune in runes.property.armor) {
                        genName = adaptiveReplace(genName, `PF2E.ArmorPropertyRune${toCamelCase(propRune)}`, runes.property.armor[propRune], g);
                    }
                }
            }

        }

        // заменим материал
        if (item.system.material.type) {
            const mat = item.system.material.type;
            genName = adaptiveReplace(genName, `PF2E.PreciousMaterial${toCamelCase(mat)}`, materials[mat], g);
        }

        // Переместим руну мощи
        const powerRune = genName.match(/^\+\d/)?.[0];
        if (powerRune)
            genName = genName.replace(powerRune, '').trim() + ' ' + powerRune;

        genName = genName[0].toUpperCase() + genName.slice(1).toLowerCase();

        return genName;
    }, 'MIXED')


}