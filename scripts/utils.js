
export function hash(s) {
    for (var i = 0, h = 9; i < s.length;)
        h = Math.imul(h ^ s.charCodeAt(i++), 9 ** 9);
    return h ^ h >>> 9
}

export async function isValidUUID(uuid, relative) {
    try {
        const parts = foundry.utils.parseUuid(uuid, relative);
        const { collection, documentId, documentType, embedded } = parts;
        if (!collection || !documentId || !collection.index.has(parts.documentId))
            return Promise.resolve(false);
        if (documentType !== "JournalEntry" || !embedded[1])
            return Promise.resolve(true);
        return collection.getDocument(parts.documentId).then(d => d.pages.some(p => p._id == embedded[1].split("#")[0]));
    }
    catch {
        return Promise.resolve(false);
    }
}

export async function findBadUUID() {
    const good = [];
    const bad = {};
    const skipped = {};

    const nmax = game.babele.translations.length + 1;
    let i = 0;
    const bars = 15;
    const message = await ChatMessage.create({ content: "", flags: {} });

    const processUUID = async (m, k) => {
        if (good.includes(m))
            return;
        if (m.startsWith(".")) {
            // Внутренняя ссылка журнала. Пока не знаю что с ней делать, поэтому сохраняем отдельно
            if (skipped[m]) {
                skipped[m].push(k);
            }
            else {
                skipped[m] = [k];
            }
            return;
        }
        if (bad[m[1]]) {
            bad[m[1]].push(k);
        }
        const g = await isValidUUID(m);
        if (g) {
            good.push(m)
        } else {
            bad[m] = [k];
        }
    }

    for (const t of game.babele.translations) {
        const b = Math.floor(i / nmax * bars);
        await message.update({ content: `<p>[${"▮".repeat(b)}${"▯".repeat(bars - b)}] ${Math.floor(i / nmax * 100)}%</p><p>${t.collection}</p>` });
        const data = flatObject(t.entries, t.collection, (s) => typeof s === "string" && s.includes("@UUID"));
        for (const k in data) {
            const v = data[k];
            for (const m of v.matchAll(/@UUID\[([^\]]+)(?:\]|$)/g)) {
                await processUUID(m[1], k);
            }
        }
        i += 1;
    };

    const loc = flatObject(game.i18n.translations, "i18n", (s) => typeof s === "string" && s.includes("@UUID"));
    for (const k in loc) {
        const v = loc[k];
        for (const m of v.matchAll(/@UUID\[([^\]]+)(?:\]|$)/g)) {
            await processUUID(m[1], k);
        }
    }
    await message.update({ content: `<p>[${"▮".repeat(bars)}] 100%</p>` })

    let content = "<h2>Сломанные ссылки</h2><table class=\"pf2e\"><tr><th>UUID</th><th>Где</th></tr>";
    for (const k in bad) {
        const v = bad[k].map(e => `<p style="user-select:all; word-break:break-word;">${e}</p>`).join("");
        content += `<tr><td style="user-select:all; word-break:break-word;">${k}</td><td>${v}</td>`;
    }
    content += "</table>";

    await message.update({ content: content });

    // console.log(bad);
    console.log(skipped);
}

export function flatObject(obj, path, filter) {
    if (typeof obj === "undefined" || obj === null)
        return ({});
    if ((typeof obj === "string" || typeof obj === "number")) {
        if (filter?.(obj) ?? true) {
            return ({ [path]: obj });
        } else {
            return ({});
        }
    }
    return Object.keys(obj).reduce((acc, k) => ({ ...acc, ...flatObject(obj[k], [path, k].join("."), filter) }), ({}));
}

function stringdistance(a, b) {
    if (!a) return b ? b.length : 0;
    else if (!b) return a.length;

    var m = a.length, n = b.length, INF = m + n, score = new Array(m + 2), sd = {};
    for (var i = 0; i < m + 2; i++) score[i] = new Array(n + 2);
    score[0][0] = INF;
    for (var i = 0; i <= m; i++) {
        score[i + 1][1] = i;
        score[i + 1][0] = INF;
        sd[a[i]] = 0;
    }
    for (var j = 0; j <= n; j++) {
        score[1][j + 1] = j;
        score[0][j + 1] = INF;
        sd[b[j]] = 0;
    }

    for (var i = 1; i <= m; i++) {
        var DB = 0;
        for (var j = 1; j <= n; j++) {
            var i1 = sd[b[j - 1]],
                j1 = DB;
            if (a[i - 1] === b[j - 1]) {
                score[i + 1][j + 1] = score[i][j];
                DB = j;
            }
            else {
                score[i + 1][j + 1] = Math.min(score[i][j], score[i + 1][j], score[i][j + 1]) + 1;
            }
            score[i + 1][j + 1] = Math.min(score[i + 1][j + 1], score[i1] ? score[i1][j1] + (i - i1 - 1) + 1 + (j - j1 - 1) : Infinity);
        }
        sd[a[i - 1]] = i;
    }
    return score[m + 1][n + 1];
}

export async function findUnmatchedLocalizations() {
    const missing = {};

    const nmax = game.babele.translations.length;
    let i = 0;
    const bars = 15;
    const message = await ChatMessage.create({ content: "", flags: {} });

    const processItem = async (key, packName, item) => {
        const pack = game.packs.get(packName);
        if (!pack)
            return;
        const idx = pack.index.find(i => (i.originalName ?? i.name) == key);
        if (idx) {
            if (pack.metadata.type === "JournalEntry") {
                for (const pk in item.pages) {
                    const n = (item.pages[pk].name ?? pk).replace("(*)", "");
                    const packEntryPages = (await pack.getDocument(idx._id)).pages;
                    if (packEntryPages.some(pp => pp.name.includes(n)))
                        continue;
                    const possibleMatches = packEntryPages.map(pp => pp.name).filter(pn => pk.includes(pn) || pn.includes(pk) || stringdistance(pk, pn) < 4)
                    if (!missing[packName])
                        missing[packName] = {};
                    missing[packName][`${key} | ${pk}`] = possibleMatches;
                }
            }
            return;
        }
        const possibleMatches = pack.index.map(i => i.originalName ?? i.name).filter(n => {
            const kl = key.toLowerCase();
            const nl = n.toLowerCase();
            return kl.includes(nl) || nl.includes(kl) || stringdistance(key, n) < 4;
        });
        if (!missing[packName])
            missing[packName] = {};
        missing[packName][key] = possibleMatches;
    }

    for (const t of game.babele.translations) {
        const b = Math.floor(i / nmax * bars);
        await message.update({ content: `<p>[${"▮".repeat(b)}${"▯".repeat(bars - b)}] ${Math.floor(i / nmax * 100)}%</p><p>${t.collection}</p>` });
        for (const k in t.entries) {
            processItem(k, t.collection, t.entries[k]);
        }
        i += 1;
    };

    await message.update({ content: `<p>[${"▮".repeat(bars)}] 100%</p>` })

    let content = "<h2>Несоответствие переводов</h2><table class=\"pf2e\"><tr><th>Название</th><th>Близкие</th></tr>";
    for (const pack in missing) {
        content += `<tr><td style="user-select:all; word-break:break-word;" colspan=2><strong>${pack}</strong></td></tr>`;
        for (const k in missing[pack]) {
            const v = missing[pack][k].map(e => `<p style="user-select:all; word-break:break-word;">${e}</p>`).join("");
            content += `<tr><td style="user-select:all; word-break:break-word;">${k}</td><td>${v}</td>`;
        }
    }
    content += "</table>";

    await message.update({ content: content });
}